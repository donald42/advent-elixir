defmodule Advent.AOC2021.Day01 do

  def gt(x,y) do
    if y > x do
      1
    else
      0
    end
  end

 
  def count_inc([x,y|t]) do
    gt(x,y) + count_inc([y | t])
  end

  def count_inc(_) do
    0
  end
  
 
  def a do

    {_, input} = AdventOfCodeHelper.get_input(2021,1)

    #input = "199\n200\n208\n210\n200\n207\n240\n269\n260\n263"


    r =
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.to_integer(x) end)


    count_inc(r)
    
  end


  def b do

    #{_, input} = AdventOfCodeHelper.get_input(2021,1)

  end

end


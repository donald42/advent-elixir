defmodule Advent.AOC2016.Day04 do

  defp is_alpha?(x) do
    x in String.split("abcdefghijklmnopqrstuvwxyz", "", trim: true)
  end

  defp ss(x), do: String.split(x, "", trim: true)

  def decode(x) do
    x
    |> String.split("", trim: true)
    |> Enum.filter(&is_alpha?/1)
    |> Enum.frequencies
    |> Enum.into([])
    |> Enum.sort_by(&elem(&1,0))
    |> Enum.sort_by(&elem(&1,1), :desc)
    |> Enum.take(5)
    |> Enum.reduce("", fn({c,_}, acc) -> acc <> c end) 
  end
  
  
  def a do

    {_, input} = AdventOfCodeHelper.get_input(2016,4)

    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> Regex.run(~r/^(.*)-(.*)\[(.*)\]$/,x, capture: :all_but_first) end)
    |> Enum.map(fn([a,b,c]) -> {decode(a),String.to_integer(b),c} end)
    #|> Enum.filter(fn({a,_,c}) -> a == c end)
    #|> Enum.map(fn({_,b,_}) -> b end)
    #|> Enum.sum

  end

  defp xform("-", _), do: " "
  defp xform(a, n) do
    a
    |> to_charlist
    |> then(fn([x]) -> Integer.mod((x-96)+n, 26)+96 end)
    |> then(fn(x) -> [x] end)
    |> to_string
  end

  def decrypt(s, n) do
    s
    |> ss
    |> Enum.map(fn(x) -> xform(x,n) end)
    |> Enum.reduce("", fn(x,acc) -> acc <> x end)
  end
  
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,4)

    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> Regex.run(~r/^(.*)-(.*)\[(.*)\]$/,x, capture: :all_but_first) end)
    |> Enum.map(fn([a,b,c]) -> {a,decode(a),String.to_integer(b),c} end)
    |> Enum.filter(fn({_,b,_,d}) -> b == d end)
    |> Enum.map(fn({a,_,n,_}) -> {n,decrypt(a, n)}  end)
    |> Enum.filter(fn({_,x}) -> Regex.match?(~r/pole/,x) end)

  end

end


defmodule Advent.AOC2016.Day01 do

  def proc_dir({:R, d}, {x, y, :N}), do: {x+d, y, :E}
  def proc_dir({:R, d}, {x, y, :E}), do: {x, y-d, :S}
  def proc_dir({:R, d}, {x, y, :S}), do: {x-d, y, :W}
  def proc_dir({:R, d}, {x, y, :W}), do: {x, y+d, :N}

  def proc_dir({:L, d}, {x, y, :N}), do: {x-d, y, :W}
  def proc_dir({:L, d}, {x, y, :W}), do: {x, y-d, :S}
  def proc_dir({:L, d}, {x, y, :S}), do: {x+d, y, :E}
  def proc_dir({:L, d}, {x, y, :E}), do: {x, y+d, :N}
 
  def a do

    {_, input} = AdventOfCodeHelper.get_input(2016,1)

    input
    |> String.split(", ", trim: true)
    |> Enum.map(fn x -> String.split_at(x,1) |> then(fn {dir, dist} -> {String.to_atom(dir), String.to_integer(String.trim(dist))} end) end)
    |> Enum.reduce({0, 0, :N}, fn (x, acc) -> proc_dir(x,acc) end)
    |> then(fn({x,y,_}) -> abs(x) + abs(y) end)

  end


#  def visit(p, m) do
#    m.put(p, :v)
#  end

#  def visited?(p, m) do
#    has_key?(m, p)
#end

  #def take_step(dir, 

 # def proc_dir2({:R, d}, {{x, y, :N}, vl}=acc), do: Enum.reduce_while(acc, fn (pos, acc) -> if visited?(pos, l), do: {:halt, }, else:   )    {{x+d, y, :E},[]}
 # def proc_dir2({:R, d}, {{x, y, :E}, vl}=acc), do: {{x, y-d, :S}, visit(}
 # def proc_dir2({:R, d}, {{x, y, :S}, vl}=acc), do: {{x-d, y, :W}, }
 # def proc_dir2({:R, d}, {{x, y, :W}, vl}=acc), do: {{x, y+d, :N}, }
#
#  def proc_dir2({:L, d}, {{x, y, :N}, vl}=acc), do: {{x-d, y, :W}, }
#  def proc_dir2({:L, d}, {{x, y, :W}, vl}=acc), do: {{x, y-d, :S}, }
#  def proc_dir2({:L, d}, {{x, y, :S}, vl}=acc), do: {{x+d, y, :E}, }
#  def proc_dir2({:L, d}, {{x, y, :E}, vl}=acc), do: {{x, y+d, :N}, }

 # def proc_step(:N, {{x,y}, l}) do
#					 end  
					 
  def b do

    #{_, input} = AdventOfCodeHelper.get_input(2016,1)

 #input
#    "R8, R4, R4, R8"
#    |> String.split(", ", trim: true)
#    |> Enum.map(fn x -> String.split_at(x,1) |> then(fn {dir, dist} -> {String.to_atom(dir), String.to_integer(String.trim(dist))} end) end)
#    |> Enum.reduce_while({{0, 0, :N},[]}, fn (x, acc) -> if x in acc, do: {:halt, x}, else: {:cont, proc_dir2(x,acc)} end)
#    |> then(fn([{x,y,_}|_]) -> abs(x) + abs(y) end)

  end

end


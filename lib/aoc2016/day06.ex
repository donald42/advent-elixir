defmodule Advent.AOC2016.Day06 do

  def a do
   
    {_, input} = AdventOfCodeHelper.get_input(2016,6)

    #input = "eedadn\ndrvtee\neandsr\nraavrd\natevrs\ntsrnev\nsdttsa\nrasrtv\nnssdts\nntnada\nsvetve\ntesnvt\nvntsnd\nvrdear\ndvrsen\nenarar"


    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.split(x, "", trim: true)  end)
    #|> Enum.reduce([[],[],[],[],[],[]], fn([a,b,c,d,e,f], [l,m,n,o,p,q]) -> [[a]++l, [b]++m, [c]++n, [d]++o, [e]++p, [f]++q]  end)
    |> Enum.reduce([[],[],[],[],[],[],[],[]], fn([a,b,c,d,e,f,g,h], [l,m,n,o,p,q,r,s]) -> [[a]++l, [b]++m, [c]++n, [d]++o, [e]++p, [f]++q, [g]++r, [h]++s]  end)    
    |> Enum.map(fn(x) -> Enum.frequencies(x) end)
    |> Enum.map(fn(x) -> Enum.sort_by(x, fn(x) -> elem(x,1) end, :desc) end)
    |> Enum.map(fn([{c,_}|_]) -> c end)
    |> Enum.join

  end

  
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,6)

    #input = "eedadn\ndrvtee\neandsr\nraavrd\natevrs\ntsrnev\nsdttsa\nrasrtv\nnssdts\nntnada\nsvetve\ntesnvt\nvntsnd\nvrdear\ndvrsen\nenarar"


    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.split(x, "", trim: true)  end)
    #|> Enum.reduce([[],[],[],[],[],[]], fn([a,b,c,d,e,f], [l,m,n,o,p,q]) -> [[a]++l, [b]++m, [c]++n, [d]++o, [e]++p, [f]++q]  end)
    |> Enum.reduce([[],[],[],[],[],[],[],[]], fn([a,b,c,d,e,f,g,h], [l,m,n,o,p,q,r,s]) -> [[a]++l, [b]++m, [c]++n, [d]++o, [e]++p, [f]++q, [g]++r, [h]++s]  end)    
    |> Enum.map(fn(x) -> Enum.frequencies(x) end)
    |> Enum.map(fn(x) -> Enum.sort_by(x, fn(x) -> elem(x,1) end) end)
    |> Enum.map(fn([{c,_}|_]) -> c end)
    |> Enum.join

  end

end


defmodule Advent.AOC2016.Day08 do

  
  def a do
   
    {_, input} = AdventOfCodeHelper.get_input(2016,8)

    
    input

  end

    
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,8)

    
    input


  end
  
end


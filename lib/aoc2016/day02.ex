defmodule Advent.AOC2016.Day02 do

  def press(1, "U"), do: 1
  def press(1, "L"), do: 1  
  def press(1, "R"), do: 2
  def press(1, "D"), do: 4

  def press(2, "U"), do: 2
  def press(2, "L"), do: 1  
  def press(2, "R"), do: 3
  def press(2, "D"), do: 5

  def press(3, "U"), do: 3
  def press(3, "L"), do: 2  
  def press(3, "R"), do: 3
  def press(3, "D"), do: 6

  def press(4, "U"), do: 1
  def press(4, "L"), do: 4  
  def press(4, "R"), do: 5
  def press(4, "D"), do: 7

  def press(5, "U"), do: 2
  def press(5, "L"), do: 4  
  def press(5, "R"), do: 6
  def press(5, "D"), do: 8

  def press(6, "U"), do: 3
  def press(6, "L"), do: 5  
  def press(6, "R"), do: 6
  def press(6, "D"), do: 9

  def press(7, "U"), do: 4
  def press(7, "L"), do: 7  
  def press(7, "R"), do: 8
  def press(7, "D"), do: 7

  def press(8, "U"), do: 5
  def press(8, "L"), do: 7  
  def press(8, "R"), do: 9
  def press(8, "D"), do: 8

  def press(9, "U"), do: 6
  def press(9, "L"), do: 8  
  def press(9, "R"), do: 9
  def press(9, "D"), do: 9


  def proc_buttons(n, dirs) do
    Enum.reduce(dirs, n, fn(x,acc) -> acc = press(acc, x)  end) 
  end

  def proc_list(_, []), do: nil
  
  def proc_list(n, [h|t]) do

    r = proc_buttons(n,h)
    IO.puts(r)
    proc_list(r, t)
  end
  
  
  def a do

    {_, input} = AdventOfCodeHelper.get_input(2016,2)

   # input = "ULL\nRRDDD\nLURDL\nUUUUD"

    i = 
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.split(x, "", trim: true) end)

    
    #i = [["U","L","L"],["R","R","D","D","D"],["L","U","R","D","L"],["U","U","U","U","D"]]
    i
    proc_list(5, i)

  end


  def press2(1, "U"), do: 1
  def press2(1, "L"), do: 1
  def press2(1, "R"), do: 1
  def press2(1, "D"), do: 3  

  def press2(2, "U"), do: 2
  def press2(2, "L"), do: 2
  def press2(2, "R"), do: 3
  def press2(2, "D"), do: 6  

  def press2(3, "U"), do: 1
  def press2(3, "L"), do: 2
  def press2(3, "R"), do: 4
  def press2(3, "D"), do: 7  

  def press2(4, "U"), do: 4
  def press2(4, "L"), do: 3
  def press2(4, "R"), do: 4
  def press2(4, "D"), do: 8  

  def press2(5, "U"), do: 5
  def press2(5, "L"), do: 5
  def press2(5, "R"), do: 6
  def press2(5, "D"), do: 5  

  def press2(6, "U"), do: 2
  def press2(6, "L"), do: 5
  def press2(6, "R"), do: 7
  def press2(6, "D"), do: :A  

  def press2(7, "U"), do: 3
  def press2(7, "L"), do: 6
  def press2(7, "R"), do: 8
  def press2(7, "D"), do: :B  

  def press2(8, "U"), do: 4
  def press2(8, "L"), do: 7
  def press2(8, "R"), do: 9
  def press2(8, "D"), do: :C  

  def press2(9, "U"), do: 9
  def press2(9, "L"), do: 8
  def press2(9, "R"), do: 9
  def press2(9, "D"), do: 9  

  def press2(:A, "U"), do: 6
  def press2(:A, "L"), do: :A
  def press2(:A, "R"), do: :B
  def press2(:A, "D"), do: :A 

  def press2(:B, "U"), do: 7
  def press2(:B, "L"), do: :A
  def press2(:B, "R"), do: :C
  def press2(:B, "D"), do: :D 

  def press2(:C, "U"), do: 8
  def press2(:C, "L"), do: :B
  def press2(:C, "R"), do: :C
  def press2(:C, "D"), do: :C 

  def press2(:D, "U"), do: :B
  def press2(:D, "L"), do: :D
  def press2(:D, "R"), do: :D
  def press2(:D, "D"), do: :D 


  def proc_buttons2(n, dirs) do
    Enum.reduce(dirs, n, fn(x,acc) -> acc = press2(acc, x)  end) 
  end

  def proc_list2(_, []), do: nil
  
  def proc_list2(n, [h|t]) do

    r = proc_buttons2(n,h)
    IO.puts(r)
    proc_list2(r, t)
  end

  
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,2)

    i = 
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.split(x, "", trim: true) end)

    

    #i = [["U","L","L"],["R","R","D","D","D"],["L","U","R","D","L"],["U","U","U","U","D"]]
    i
    proc_list2(5, i)


  end

end


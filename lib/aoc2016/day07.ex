defmodule Advent.AOC2016.Day07 do

  defp abba?(x) do
    x
    |> Enum.map(fn(x) ->  Regex.scan(~r/(?=(\w\w\w\w))/, x, capture: :all_but_first) end)
    |> List.flatten
    |> Enum.map(&String.split(&1,"", trim: true))
    |> Enum.reduce(false, fn([a,b,c,d], acc) -> (a == d and b == c and a != b) or acc  end)
  end

  defp outbrack(x) do
    x
    |> then(fn(x) -> Regex.replace(~r/\[[^[]*\]/, x, "-", global: true) end)
    |> String.split("-", trim: true)
  end
      

  defp inbrack(x) do
     x
     |> then(&Regex.replace(~r/\]/, &1, "-", global: true))    
     |> then(&Regex.replace(~r/\[/, &1, ":", global: true))    
     |> then(&Regex.replace(~r/-/, &1, "[", global: true))
     |> then(&Regex.replace(~r/:/, &1, "]", global: true))
     |> then(fn(x) -> "[" <> x <> "]" end)
     |> then(&outbrack/1)
  end
  
  
  def a do
   
    {_, input} = AdventOfCodeHelper.get_input(2016,7)

    #input = "abba[mnop]qrst\nabcd[bddb]xyyx\naaaa[qwer]tyui\nioxxoj[asdfgh]zxcvbn\nabcdef[jyrj]asdfa[klmn]shtthur"
    
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> abba?(outbrack(x)) and not abba?(inbrack(x)) end)
    |> Enum.filter(fn(x) -> x end)
    |> Enum.count
  end

  defp aba(x) do
    x
    |> Enum.map(fn(x) ->  Regex.scan(~r/(?=(\w\w\w))/, x, capture: :all_but_first) end)
    |> List.flatten
    |> Enum.uniq
    |> Enum.map(fn(x) -> String.split(x, "", trim: true) end)
    |> Enum.filter(fn([a,b,c]) -> a == c and a != b end)
    |> Enum.map(fn(x) -> Enum.join(x) end)
  end

  defp bab_flip(x) do
    x
    |> aba
    |> Enum.map(fn(x) -> String.split(x, "", trim: true) end)
    |> Enum.map(fn([a,b,_]) -> Enum.join([b,a,b]) end)
  end

  def disjoint_lists?(a,b) do
    MapSet.disjoint?(MapSet.new(a), MapSet.new(b))
  end
    
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,7)

    #input = "aba[bab]xyz\nxyx[xyx]xyx\naaa[kekst]eke\nzazbz[bzb]cdb"
    
    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> not disjoint_lists?(x |> outbrack |> aba, x |> inbrack |> bab_flip) end)
    |> Enum.filter(fn(x) -> x end)
    |> Enum.count

  end
  
end


defmodule Advent.AOC2016.Day05 do

  #def rproc(_, 3231930), do: ""

  def hash(x), do: :crypto.hash(:md5, x) |> Base.encode16()
  
  def rproc(s, i) do

    x = hash(s <> to_string(i))

    if String.starts_with?(x, "00000") do
      {i, String.at(x,5)}
    else
      rproc(s, i+1)
    end
    
  end

  def proc(_, _, 8), do: []
  def proc(s, i, n) do

    {a,c} = rproc(s,i)
    [{a,c}] ++ proc(s, a+1, n+1)

  end

  
  def a do

    {_, input} = AdventOfCodeHelper.get_input(2016,5)

    #input = "abc"
    
    input
    |> String.trim
    |> proc(0, 0)
    |> Enum.reduce("", fn({_, c}, acc) -> acc <> c end)

  end

  defp num(s) do
    String.to_integer(s)
  end
  
  
  defp num?(s) do
    s in ["1","2","3","4","5","6","7","8","9","0"]
  end

  def rproc2(s, i, l) do

    x = hash(s <> to_string(i))

    if String.starts_with?(x, "00000") and
    num?(String.at(x,5)) and
    num(String.at(x,5)) >= 0 and
    num(String.at(x,5)) <= 7 and
    num(String.at(x,5)) not in l do
      
      {i, x, [num(String.at(x,5))] ++ l,num(String.at(x,5)), String.at(x, 6)}
    else
      rproc2(s, i+1, l)
    end
    
  end


  def proc2(_, _, _, 8), do: []
  def proc2(s, i, l, n) do

    {a,x,l,b,c} = rproc2(s, i, l)

    [{a,x,l,b,c}] ++ proc2(s, a+1, l, n+1)

  end

  
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,5)

    #input = "abc"
    
    input
    |> String.trim
    |> proc2(0, [], 0)
    |> Enum.sort_by(fn({_,_,_,c,_}) -> c end)
    |> Enum.reduce("", fn({_, _, _, _, c}, acc) -> acc <> c end)

  end

end


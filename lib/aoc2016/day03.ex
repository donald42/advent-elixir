defmodule Advent.AOC2016.Day03 do


  def poss_tri?([a,b,c]) do

    a + b > c and
    b + c > a and
    a + c > b
    
  end
  
 
  def a do

    {_, input} = AdventOfCodeHelper.get_input(2016,3)

    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.split(x, ~r/ +/, trim: true) |> Enum.map(fn(x) -> String.to_integer(x) end) end)
    |> Enum.filter(&poss_tri?(&1))
    |> Enum.count

  end



					 
  def b do

    {_, input} = AdventOfCodeHelper.get_input(2016,3)

    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn(x) -> String.split(x, ~r/ +/, trim: true) |> Enum.map(fn(x) -> String.to_integer(x) end) end)
    |> Enum.chunk_every(3)
    |> Enum.reduce([], fn([[a,b,c],[d,e,f],[g,h,i]], l) -> [[a,d,g]] ++ [[b,e,h]] ++ [[c,f,i]] ++ l end)
    |> Enum.filter(&poss_tri?(&1))
    |> Enum.count

  end

end


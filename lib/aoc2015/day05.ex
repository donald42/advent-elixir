defmodule Advent.AOC2015.Day05 do

   def three_vowels?(str) do
     str
     |> String.split("", trim: true)
     |> Enum.filter(fn(x) -> String.contains?(x, ["a","e","i","o","u"]) end)
     |> length >= 3
   end

   def double_letter?(str) do
     String.match?(str, ~r/(.)\1/)
   end
   
   def no_forbidden_strings?(str) do
     not String.contains?(str, ["ab", "cd", "pq", "xy"])
   end
   
   def nice_string_1?(str) do
     three_vowels?(str) and double_letter?(str) and no_forbidden_strings?(str)
   end

   def double_match?(str) do
     String.match?(str, ~r/(..).*\1/)
   end
   
   def aba?(str) do
     String.match?(str, ~r/(.).\1/)
   end

   def nice_string_2?(str) do
     double_match?(str) and aba?(str)
   end
   
   def a do
     {_, input} = AdventOfCodeHelper.get_input(2015,5)

     input
     |> String.split("\n", trim: true)
     |> Enum.count(fn(x) -> nice_string_1?(x) end)
   end
   
   def b do
     {_, input} = AdventOfCodeHelper.get_input(2015,5)

     input
     |> String.split("\n", trim: true)
     |> Enum.count(fn(x) -> nice_string_2?(x) end)
   end

end


defmodule Advent.AOC2015.Day03 do

   def proc_dirs(dirlist) do
     dirlist
     |> Enum.reduce([{0,0}], fn (dir, [{x,y} | _] = lst) ->
	case dir do
	  "<" ->
	    [{x-1,y} | lst]
	  ">" ->
	    [{x+1,y} | lst]
	  "^" ->
	    [{x,y+1} | lst]
	  "v" ->
	    [{x,y-1} | lst]
	end
      end)
   end
   
   def a do
      {_, input} = AdventOfCodeHelper.get_input(2015,3)

      input
      |> String.split("", trim: true)
      |> proc_dirs
      |> Enum.uniq
      |> Enum.count
   end

   def list_split(dirlist) do
     dirlist
     |> String.split("", trim: true)
     |> Enum.chunk_every(2)
     |> Enum.reduce({[],[]}, fn [h | t], {s,r} -> {s ++ [h], r ++ t} end)
     |> then(fn({a,b}) -> [a,b] end)
   end
   
   def b do
     {_, input} = AdventOfCodeHelper.get_input(2015,3)
      
     input
     |> list_split
     |> Enum.map(fn(x) -> x |> proc_dirs end)
     |> List.flatten
     |> Enum.uniq
     |> Enum.count
   end

end

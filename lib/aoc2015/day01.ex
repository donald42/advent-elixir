defmodule Advent.AOC2015.Day01 do

   def a do
      {_, input} = AdventOfCodeHelper.get_input(2015,1)

      input
      |> String.to_charlist
      |> Enum.map(fn(t) -> if t == 40, do: 1, else: -1 end)
      |> Enum.sum
   end

   def b do
      {_, input} = AdventOfCodeHelper.get_input(2015,1)

      input 
      |> String.to_charlist
      |> Enum.map(fn(t) -> if t == 40, do: 1, else: -1 end)
      |> Enum.reduce_while({0,0}, fn x, {v,c}=acc -> if v == -1, do: {:halt, acc}, else: {:cont, {v + x, c + 1}} end)
   end

end


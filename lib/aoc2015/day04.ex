defmodule Advent.AOC2015.Day04 do

   def find_hash_with_prefix(salt, prefix, i) do

     md5sum = 
     salt <> Integer.to_string(i)
     |> then(fn(x) -> :crypto.hash(:md5, x) end)
     |> Base.encode16

     if String.starts_with?(md5sum, prefix) do
       i
     else
       find_hash_with_prefix(salt, prefix, i+1)
     end
     
   end
   
   
   def a do
     {_, input} = AdventOfCodeHelper.get_input(2015,4)

     find_hash_with_prefix(String.trim(input), "00000", 0)
   end
   
   def b do
     {_, input} = AdventOfCodeHelper.get_input(2015,4)

     find_hash_with_prefix(String.trim(input), "000000", 0)
   end

end


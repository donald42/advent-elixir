defmodule Advent.AOC2015.Day06B do


  def get_light_range(rng) do
    String.split(rng, " through ", trim: true)
    |> Enum.map(fn(x) -> String.split(x, ",", trim: true) end)
    |> Enum.map(fn([a,b]) -> {String.to_integer(a), String.to_integer(b)} end)
  end
  
  
  def proc_lights("turn on " <> rng) do
    [{sx, sy}, {ex, ey}] = get_light_range(rng)

    for x <- (sx..ex), y <- (sy..ey), do: :ets.insert(:lights, {{x,y}, :on})
  end
  
  def proc_lights("turn off " <> rng) do
    [{sx, sy}, {ex, ey}] = get_light_range(rng)

    for x <- (sx..ex), y <- (sy..ey), do: :ets.delete(:lights, {x,y})    
  end

  def proc_lights("toggle " <> rng) do
    [{sx, sy}, {ex, ey}] = get_light_range(rng)

    
    for x <- (sx..ex), y <- (sy..ey) do
      if (length(:ets.lookup(:lights, {x,y})) == 0) do
	:ets.insert(:lights, {{x,y}, :on})
      else
	:ets.delete(:lights, {x,y})
      end
    end
    
  end


  
  def a do
    {_, input} = AdventOfCodeHelper.get_input(2015,6)

    #lights = %{}
    :ets.new(:lights, [:set, :named_table])
    

    input
    |> String.split("\n", trim: true)
    |> Enum.map(fn x -> proc_lights(x) end)
    #|> Enum.count

    IO.puts Enum.count(:ets.match(:lights, {:_, :on}))

    :ets.delete(:lights)

    
  end


  def proc_lights2("turn on " <> rng, lights) do
    [{sx, sy}, {ex, ey}] = get_light_range(rng)

    cng = for x <- (sx..ex), y <- (sy..ey), do: {x,y}

    cng
    |> Enum.reduce(lights, fn x, acc -> Map.put(acc, x, Map.get(acc,x) + 1) end)
  end
  
  def proc_lights2("turn off " <> rng, lights) do
    [{sx, sy}, {ex, ey}] = get_light_range(rng)
    
    cng = for x <- (sx..ex), y <- (sy..ey), do: {x,y}

    cng
    |> Enum.reduce(lights, fn x, acc -> Map.put(acc, x, (if Map.get(acc, x) > 0, do: Map.get(acc, x) - 1, else: 0)) end)    
  end

  def proc_lights2("toggle " <> rng, lights) do
    [{sx, sy}, {ex, ey}] = get_light_range(rng)

    cng = for x <- (sx..ex), y <- (sy..ey), do: {x,y}

    cng
    |> Enum.reduce(lights, fn x, acc -> Map.put(acc, x, Map.get(acc,x) + 2) end)
  end

  
  def b do
    {_, input} = AdventOfCodeHelper.get_input(2015,6)

    lights =     
    for x <- (0..999), y <- (0..999), into: %{}, do: {{x,y}, 0}

    input
    |> String.split("\n", trim: true)
    |> Enum.reduce(lights, fn x,acc -> proc_lights2(x, acc) end)
    |> Map.values
    |> Enum.sum


  end

end


defmodule Advent.AOC2015.Day02 do

   def calc_paper([a,b,c]) do
      (2*a*b) + (2*b*c) + (2*a*c) + (a*b)
   end

   def calc_ribbon([a,b,c]) do
      (2*a) + (2*b) + (a*b*c)
   end

   def apply_calc(x, f) do
      x 
      |> String.split("x", trim: true)
      |> Enum.map(&String.to_integer/1)
      |> Enum.sort
      |> then(f)
   end

   def a do
      {_, input} = AdventOfCodeHelper.get_input(2015,2)

      input
      |> String.split("\n", trim: true)
      |> Enum.map(fn(x) -> apply_calc(x, &calc_paper/1) end)
      |> Enum.sum
   end

   def b do
      {_, input} = AdventOfCodeHelper.get_input(2015,2)

      input
      |> String.split("\n", trim: true)
      |> Enum.map(fn(x) -> apply_calc(x, &calc_ribbon/1) end)
      |> Enum.sum
   end

end

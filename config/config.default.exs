use Mix.Config

# Rename this to "config.exs" and put your AOC session cookie below

config :advent_of_code_helper, 
session: "<your session cookie here>",
cache_dir: ".cache/" 
